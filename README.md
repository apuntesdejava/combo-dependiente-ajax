# Combo dependiente con JSP + Ajax #

Este es el código fuente del post publicado aquí: http://www.apuntesdejava.com/2009/05/combo-dependiente-con-jsp-ajax.html

Aquí utiliza:

* Maven (para obtener las bibliotecas necesarias)
* Spring (para el DAO | Factory)
* Apache Derby (para la base de datos. Usa la base de datos "sample" que viene de ejemplo)
* Servlets + JSON (para proporcionar la comunicación ajax
* JQuery (para la parte visual)
* AngularJS (una alternativa a JQuery)
* BootStrap (para que sea vea bonito con JQuery)
* Material Design (para que se vea bonito con AngularJS)
