/*
 * Copyright 2016 dsilva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.combo.dependiente.ajax.dao.jdbc;

import com.apuntesdejava.combo.dependiente.ajax.dao.ProductCodeDao;
import com.apuntesdejava.combo.dependiente.ajax.domain.ProductCode;
import java.sql.ResultSet;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author dsilva
 */
public class JdbcProductCodeDao implements ProductCodeDao {

    /**
     * Esta es la plantilla JDBC lista para utilizar
     */
    private JdbcTemplate jdbcTemplate;

    @Override
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<ProductCode> getAllProductCodeList() {
        //con JDBC template puedo hacer un query, prepara la lista devuelva como arreglo con objetos instanciados
        return jdbcTemplate.query("SELECT * FROM PRODUCT_CODE", (ResultSet rs, int i) -> {
            //Este es el método que se encarga de mapear cada objeto de ResultSet y lo convierte en ProductCode
            ProductCode productCode = new ProductCode(
                    rs.getString("PROD_CODE"),
                    rs.getString("DISCOUNT_CODE"),
                    rs.getString("DESCRIPTION"));
            return productCode;
        });
    }
}
