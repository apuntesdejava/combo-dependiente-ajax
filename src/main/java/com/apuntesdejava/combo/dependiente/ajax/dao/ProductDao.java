/*
 * Copyright 2016 dsilva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.combo.dependiente.ajax.dao;

import com.apuntesdejava.combo.dependiente.ajax.domain.Product;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author dsilva
 */
public interface ProductDao {

    /**
     * Establece el datasource previamente conectado a la base de datos
     *
     * @param dataSource
     */
    void setDataSource(DataSource dataSource);

    /**
     * Obtiene una lista de {@link Product} filtrado por el campo productCode
     * dado por argumento {@code productCode}
     *
     * @param productCode
     * @return
     */
    List<Product> findByProductCode(String productCode);
}
