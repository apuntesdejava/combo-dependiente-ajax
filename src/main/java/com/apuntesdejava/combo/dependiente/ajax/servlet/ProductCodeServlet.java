/*
 * Copyright 2016 dsilva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.combo.dependiente.ajax.servlet;

import com.apuntesdejava.combo.dependiente.ajax.dao.DaoFactory;
import com.apuntesdejava.combo.dependiente.ajax.dao.ProductCodeDao;
import com.apuntesdejava.combo.dependiente.ajax.domain.ProductCode;
import java.io.IOException;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author dsilva
 */
@WebServlet(urlPatterns = "/ProductCodeServlet")
public class ProductCodeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //obtenemos del dao
        ProductCodeDao productCodeDao = DaoFactory.getInstance().getProductCodeDao();
        //obtenemos la lista de productos
        List<ProductCode> productCodeList = productCodeDao.getAllProductCodeList();
        //debemos preparar la lista a devolver. 
        //Por los actuales estándares, es mejor usar JSON
        JsonArrayBuilder array = Json.createArrayBuilder();//creamos el arreglo json
        //recorremos la lista
        for (ProductCode productCode : productCodeList) {
            //creamos un objeto json con los campos que necesitamos
            JsonObject item = Json.createObjectBuilder()
                    .add("prodCode", productCode.getProdCode())
                    .add("discountCode", productCode.getDiscountCode())
                    .add("description", productCode.getDescription()).build();
            array.add(item); //.. y lo agregamos al arreglo json
        }
        /*Lo mismo, pero usando Lambda
                productCodeList.stream().map((productCode) -> Json.createObjectBuilder()
                .add("prodCode", productCode.getProdCode())
                .add("discountCode", productCode.getDiscountCode())
                .add("description", productCode.getDescription()).build()).forEach((item) -> {
            //creamos un objeto json con los campos que necesitamos
            array.add(item); //.. y lo agregamos al arreglo json
        });
         */

        resp.setContentType(MediaType.APPLICATION_JSON); //ahora preparamos la salida json al cliente...
        try (JsonWriter jsonWriter = Json.createWriter(resp.getOutputStream())) { //.. para imprimir... 
            jsonWriter.writeArray(array.build());
        }

    }

}
