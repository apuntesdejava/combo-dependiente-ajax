/*
 * Copyright 2016 dsilva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.combo.dependiente.ajax.dao;

import com.apuntesdejava.combo.dependiente.ajax.domain.ProductCode;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dsilva
 */
public class ProductCodeTest {

    private ProductCodeDao dao;

    public ProductCodeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        dao = DaoFactory.getInstance().getProductCodeDao();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void produceCodeList() {
        List<ProductCode> list = dao.getAllProductCodeList();
        assertFalse(list.isEmpty());
    }

}
