/*
 * Copyright 2016 dsilva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.combo.dependiente.ajax.servlet;

import com.apuntesdejava.combo.dependiente.ajax.dao.DaoFactory;
import com.apuntesdejava.combo.dependiente.ajax.dao.ProductDao;
import com.apuntesdejava.combo.dependiente.ajax.domain.Product;
import java.io.IOException;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author dsilva
 */
@WebServlet(urlPatterns = "/ProductServlet")
public class ProductServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //el dao..
        ProductDao productDao = DaoFactory.getInstance().getProductDao();
        //el argumento
        String productCode = req.getParameter("productCode");
        //la lista
        List<Product> productList = productDao.findByProductCode(productCode);
        //de la misma manera, crearemos un arreglo json
        JsonArrayBuilder array = Json.createArrayBuilder();

        //esta vez recorremos la lista como lambda
        productList.stream().map((product) -> Json.createObjectBuilder()
                .add("productId", product.getProductId())
                .add("purchaseCost", product.getPurchaseCost())
                .add("description", product.getDescription()).build()).forEach((item) -> {
            array.add(item);
        });
        resp.setContentType(MediaType.APPLICATION_JSON);//tipo application/json
        try (JsonWriter jsonWriter = Json.createWriter(resp.getOutputStream())) {
            jsonWriter.writeArray(array.build());
        }
    }
    
}
