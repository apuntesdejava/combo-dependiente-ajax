/*
 * Copyright 2016 dsilva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.combo.dependiente.ajax.dao.jdbc;

import com.apuntesdejava.combo.dependiente.ajax.dao.ProductDao;
import com.apuntesdejava.combo.dependiente.ajax.domain.Product;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author dsilva
 */
public class JdbcProductDao implements ProductDao {

    /**
     * Esta es la plantilla JDBC lista para utilizar
     */
    private JdbcTemplate jdbcTemplate;

    @Override
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Product> findByProductCode(String productCode) {
        //El query, el mapeador...
        return jdbcTemplate.query("SELECT * FROM PRODUCT WHERE PRODUCT_CODE = ?", new RowMapper<Product>() {
            @Override
            public Product mapRow(ResultSet rs, int i) throws SQLException {
                return new Product(
                        rs.getInt("PRODUCT_ID"),
                        rs.getDouble("PURCHASE_COST"),
                        Boolean.valueOf(rs.getString("AVAILABLE")),
                        rs.getString("DESCRIPTION"));
            }
        }, productCode); //y el parámetro
        
        /*Lo mismo, pero con Lambda
           return jdbcTemplate.query("SELECT * FROM PRODUCT WHERE PRODUCT_CODE = ?", (ResultSet rs, int i) -> new Product(
                rs.getInt("PRODUCT_ID"),
                rs.getDouble("PURCHASE_COST"),
                Boolean.valueOf(rs.getString("AVAILABLE")),
                rs.getString("DESCRIPTION")), productCode);
        */
    }

}
