/*
 * Copyright 2016 dsilva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.combo.dependiente.ajax.dao;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author dsilva
 */
public class DaoFactory {

    private final BeanFactory factory;
    
    private DaoFactory() {
        ApplicationContext ctx=new ClassPathXmlApplicationContext("spring.xml");
        factory=(BeanFactory)ctx;
    }
    
    public static DaoFactory getInstance() {
        return DaoFactoryHolder.INSTANCE;
    }
    
    private static class DaoFactoryHolder {

        private static final DaoFactory INSTANCE = new DaoFactory();
    }
    /**
     * Obtiene un bean instanciado para {@link ProductCodeDao}
     * @return 
     */
    public ProductCodeDao getProductCodeDao(){
        return factory.getBean(ProductCodeDao.class);
    }
    /**
     * Obtiene un bean instanciado para {@link ProductDao}
     * @return 
     */
    
    public ProductDao getProductDao(){
        return factory.getBean(ProductDao.class);
    }
}
